public class City
{
	public static void main(String[] args) 
	{
		System.out.println("Here are the cities that i've lived in:");
		System.out.println(" ");
		System.out.println("Kristianstad - This is where I was born, and where I've lived most of my life!");
		System.out.println("Växjö - This is where I went to university, and where I'm currently living!");
		System.out.println("Hatfield -  A smaller city outside of London, I lived here during my semester abroad in 2017.");
	}
}